# Exercise 02-S2: Gradient descent with least squares [[PDF](SS21-MsAML__03-XS2__Gradient_descent_least_squares.pdf)]

Suppose that we use gradient descent to solve the linear regression problem with squared loss 

$$\hat{R}(S,\beta) = \|X\beta - Y \|_2^2 \to \text{min}_\beta$$

where the notation is the same as in Exercise 02-XS1. Suppose also that the features matrix $X$ has full column rank.

We initialize the parameters vector by $\beta^{(0)}=0$ and set a fixed learning rate $\eta$, such that 

$$ 0 < \eta \leq \frac{1}{\lambda_{max}(X^TX)}$$

where $\lambda_{max}(X^TX)$ is the largest eigenvalue of $X^TX$.

Prove that in this setting gradient descent converges to the analytical solution of ordinary least squares problem, i.e. 

$$\text{lim}_{k\to\infty} \beta^{(k)} = \hat{\beta} = (X^TX)^{-1}X^TY$$

where $\beta^{(k)}$ is the vector of parameters at the $k$-th iteration of gradient descent.








