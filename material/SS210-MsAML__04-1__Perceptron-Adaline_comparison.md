# MsAML-04-1 [[PDF](SS210-MsAML__04-1__Perceptron-Adaline_comparison.pdf)]

1. [Perceptron-Adaline comparison](#perceptron-adaline-comparison)
   * [Online, batch, and mini-batch learning](#online,-batch,-and-mini-batch-learning)

Back to [index](index.md).


## Perceptron-Adaline comparison

Our main motivation in replacing the Perceptron by the Adaline algorithm were:

1. For linearly separable training data, the Perceptron stops its updates right
   after the last incorrect classification is resolved by the last parameter
   update. Whether this last parameter update is in any sense optimal with
   respect to the generalization performance is not addressed by the algorithm,
   and there is no mechanism to even specify a notion for the possible
   generalization.
2. For "almost" linearly separable training data, the Perceptron jumps back an
   forth in its parameter updates depending on the found classification errors
   instead of nevertheless attempting to find an optimal hyperplane.

In turn, the Adaline neuron allows to specify a notion of generalization by
means of the minima of the respective empirical loss 
\begin{align}
  \widehat L(w,b) = \frac1N \sum_{i=1}^N L(y^{(i)},h_{(w,b)}(x^{(i)}))
\end{align}
for given loss function $L(w,b)$ and activation output $h_{(w,b)}$ parametrized
by $w,b$.

**Ad 1. above:** Even in case the gradient descent algorithm of the Adaline
managed to find parameters $w,b$ that imply a zero total amount of
classification errors, the empirical loss may not yet be close to a local
minimum. And therefore, a continued gradient descent may bring the parameters
$w,b$ even closer to this empirical minimum.

For the choice of a square error loss one may encounter the following scenario:

![Perceptron-Adaline updates.](04/Perceptron_Adaline_stops.png)

If the notion of generalization encoded in the loss function resembles our
learning tasks better the Adaline has a good chance to generalize better. This
is illustrated in the following figure, in which the crosses denote the training data and
the dots the unseen test data.

![Potential generalization performance of the Adaline.](04/Adaline_generalization.png)

Though both hyperplanes have no classification errors, the one on the
right-hand side seems to generalizes better -- at least judged by the "unseen"
test data.  Here it is important, not to be fooled by any apparent symmetry as
depicted in the figures as it may have been due to the unfairly chosen batch of
the training data. 

**Ad 2. above:** Also in the case of training data that is not linearly
separable but there are hyperplanes that allow a classification with only few
errors, the Adaline has a good chance finding a compromise between maximizing the
about of correctly classified training data points and minimizing the
misclassifications. Again this strongly depends on whether the notion of
generalization supplied by the loss functions resembles the learning task to
some a sufficient extend.

The desirable generalization performance depends on several ingredients: 

* The Adaline attempts to minimize the empirical loss $\widehat L(w,b)$ and not
  the total number of classification errors. Therefore, $L$ should be chosen in
  a way that the minimization of $\widehat L(w,b)$ also enforces the
  minimization of the total number of classification errors. At best, it should
  encode as much a priori knowledge that we have at hand about the learning
  task.
* Furthermore, $L$ must be chosen sufficiently regular to allow for the
  gradient descent method to work. But even then we are not guaranteed to end
  up in a global minimum as the following discussion shows.

**Scenario 1:** For small learning rates $\eta$ and model parameters $w,b$
close to a local minimum of the empirical loss, the gradient descent might get
stuck at a local minimum instead of finding a potentially global one:

![Gradient descent getting caught in local minimum due too a small learning rate $\eta$.](04/Caught_in_local_min.png)

Even if we do not get stuck at a local minimum before descending to a global
one, a too small choice of $\eta$ may require a large amount of training epochs
(iterations in the gradient descent algorithm).

**Scenario 2:** In turn, a too large learning rate $\eta$ may easily cause the
gradient descent algorithm to overshoot even a global minimum and potentially
lead to an increases empirical loss as illustrated here:

![Overshooting a minimum due to a too large learning rate.](04/Overshooting_a_minimum.png)

Both scenarios are the price for introducing an ad hoc constant learning rate.
As it could already be seen from the exercises, the success of the gradient
descent depends on the sign and magnitude of the Taylor remainder beyond the
linear correction of the empirical loss, which can be estimated by the
corresponding Hessian.  Therefore, more sophisticated algorithms typically
rely on the second-order derivatives; e.g., most prominently the Newton's
method.  Computing the second-order derivative is of course not a problem for
our single Adaline application. However, later on, when considering networks
of Adaline neuron, the computation of second-order derivatives of the loss
function becomes computationally very costly.

Therefore, despite the discussed problems, gradient descent is the method of
choice for training larger networks of Adaline neurons and the fine-tuning of
learning rates $\eta$ and loss functions $L$ that work together well will often
require a lot of finesse and patience.

Furthermore, there are several improvements that can be made when allowing an
adaptive learning rate during the gradient descent optimization. For example,
in the $t$-th learning epoch we might adjust the learning rate according to
\begin{align}
  \eta = \frac{a}{b+t},\qquad a,b>0.
\end{align}
This implies that $\eta(t)$ is larger for early epochs $t$ and smaller for
later ones -- then, it ultimately enforce a convergence, though, not
necessarily at a local minimum.

More sophisticated methods, e.g., implement a Newtonian dynamics for a
fictional body moving in parameter space of $(w,b)$, interpreting $\widehat
L(w,b)$ as a "potential" that exerts a force proportional to
$-\nabla_{(w,b)}\widehat L(w,b)$ on that body, and in addition, introducing a
friction that damps the body's velocity continuously. The inherent inertia of the
body then enables it to "roll" out of local minima unless they are sufficiently
deep for the friction to take over.  Others implement, in addition to the
gradient descent, a random motion with decreasing temperature. The latter allows
to randomly "jump" out of local minima unless they are sufficiently deep so
that the remaining temperature does not allow for a sufficiently large jump
anymore in order to escape.

You can find a great overview of some of these methods here: 
<http://sebastianruder.com/optimizing-gradient-descent/>

In summary, while the success of a supervised training will always depend on
some finesse in the fine-tuning of the additional parameters of our
optimization algorithm of choice for minimizing the empirical loss, a
advantageous choice of loss function will require some a priori knowledge that
cannot be extracted "for free" from simply looking at the known training alone.
This fact will become soberingly clear in our next module about the "No free
lunch" theorems.


### Online, batch, and mini-batch learning

There is another noteworthy difference in the update rules of the Perceptron
and the Adaline neurons; at least in the way we have introduced them.

* The Perceptron update rule is an *online learner* as the model parameter
  updates occur right after inspection of a single training data points. 
* The Adaline update rule is a *batch learner* as it first considers the whole
  batch of training data by computing the gradient of the empirical loss
  $\widehat L(w,b)$ before then updating the model parameters.

While online learner typically introduce a stronger dependence on the sequence
of training data, batch learner are more computationally expensive.

Clearly, also the Perceptron can be turned into a batch learner by conducting
only one average parameter update per epoch, i.e., after the inspection of the
entire batch of training data. Likewise, we may turn our Adaline implementation
into an online learner by performing for each inspected training data point
$(x^{(i)},y^{(i)})$ an immediate parameter update according to the loss
function $L$, i.e., 
\begin{align}
  w & \leftarrowtail w^\text{new} := w - \eta \frac{\partial L(y^{(i)},h_{(w,b)}(x^{(i)}))}{\partial w}\\
  b & \leftarrowtail b^\text{new} := b - \eta \frac{\partial L(y^{(i)},h_{(w,b)}(x^{(i)}))}{\partial b},
\end{align}
instead of the using the empirical loss $\widehat L(w,b)$ of entire batch.

A compromise between these two extremes are the so-called *mini-batch*
learners, which, per learning epoch, divide their training data
$s=(x^{(i)},y^{(i)})_{i=1,\ldots,N}$ into disjoint mini-batches $s_k$,
$k=1,\ldots,M$, each containing a similar amount of training data samples and
execute the update rules after inspection of each such mini-batch $s_k$. In
order to reduce the dependence on the sequence, this division can be done
randomly. This is also today's most common method of training also larger
Adaline networks. When the gradient descent algorithm is used in combination of
such a randomized mini-batch approach, it is often referred to as *stochastic
gradient descent*.
