# SS21-MsAML 01-3 [[PDF](SS21-MsAML__01-3__Supervised_learning_setting.pdf)]

1. [Supervised learning setting](#supervised-learning-setting)

Back to [index](index.md).


## Supervised learning setting

We start our general discussion on supervised machine learning with the help of a traditional machine learning data set, the Iris flow data set:

* <http://archive.ics.uci.edu/ml/datasets/iris>

This data set comprises measurements of three types of Iris flower species ([Source](https://en.wikipedia.org/wiki/Iris_flower_data_set)):

![Iris setosa](https://upload.wikimedia.org/wikipedia/commons/5/56/Kosaciec_szczecinkowaty_Iris_setosa.jpg){width=80%}

![Iris versicolor](https://upload.wikimedia.org/wikipedia/commons/4/41/Iris_versicolor_3.jpg){width=80%}

![Iris virginica](https://upload.wikimedia.org/wikipedia/commons/9/9f/Iris_virginica.jpg){width=80%}

In the following we will look at the measurements of sepal length and width:

![Iris virginica](./01/Iris_sepal_width_length.png){width=80%}

The following shows a scatter plot of the two dimensional data tuple of sepal length and width:

![Iris data scatter plot of sepal width and length.](./01/iris_plot.svg)

Each data point is annotated by a color that encodes the Iris flower species
from which the measurement had been taken.

Suppose we want an algorithm to learn the classification of a Iris species type
by means of given examples of sepal length and width data which are already
annotated by means of the Iris species type. This is a typical machine learning
which, more abstractly, can be cast into the following form:

Goal 
:   We seek an algorithm $\mathcal A$ with the following specification:
    
    **Input:** Training data $s=(x^{(i)},y^{(i)})_{i=1\ldots N}$ of length $N$
    where we call:
    
    * $x^{(i)}$ a feature in feature space $\mathcal X$;
    * $y^{(i)}$ a label in label space $\mathcal Y$;
    
    **Output:** A map $h:\mathcal X\to\mathcal Y$ called hypothesis that, for any $x\in\mathcal X$
    produces a prediction of a label $h(x)\in\mathcal Y$.

In our example of the Iris data set, it would be convenient to choose

\begin{align}
  \mathcal X=\mathbb R^2
\end{align}

to represent our data tuples as vectors:

\begin{align}
  x^{(i)}
  =
  \begin{pmatrix}
    x^{(i)}_1 \\
    x^{(i)}_2
  \end{pmatrix}
  \quad
  \begin{array}{l}
    \leftarrow \text{sepal length}\\
    \leftarrow \text{sepal width}
  \end{array}
\end{align}

and a label space

\begin{align}
  \mathcal Y = \{0, 1, 2\},
\end{align}

where the integers 0, 1, 2 encode the species "Iris setosa", "Iris versicolor", "Iris virginica", respectively.

For other tasks we would have to adapt feature and label spaces $\mathcal X, \mathcal Y$ accordingly. To imagine other example, let us consider the task of object recognition from images. For this purpose
the feature space could be chosen consist of matrices
\begin{align}
  \mathcal X = [0,1]^{m\times n}
\end{align}
representing gray scale images of $m\times n$ pixels.  Furthermore, the label space may comprise strings encoding the object classes, e.g.,
\begin{align}
  \mathcal Y = \{\text{human}, \text{cat}, \text{car}, \ldots\}.
\end{align}


In this lecture 
:   we will mainly discuss and implement such algorithms $\mathcal A$ and analyze
    how they perform. Note that  the algorithm itself can be viewed as a map from
    sets of training data $s$ to a "learned" hypothesis 
    \begin{align}
      h_s\in\mathcal Y^{\mathcal X}
    \end{align}
    or, more precisely,
    \begin{align}
      \mathcal A: \bigcup_{N\in\mathbb N} \left(\mathcal X\times \mathcal Y\right)^N 
      & \to 
      \mathcal Y^{\mathcal X}
      \\
      s
      & \mapsto 
      h_s.
    \end{align}
    Here, the $\mathcal Y^{\mathcal X}$ is short for all maps $\mathcal X\to\mathcal Y$.

Before discussing ideas of how such algorithms can "learn" a hypothesis to predict labels for unseen samples from a set of known training data, let us consider what we would expect from the "learned" hypothesis:

1. It should make as few errors as possible when classifying the known training data.
2. It should *generalize* well to previously unseen samples.

Especially the second point is naturally very difficult to control as little to
nothing might be known about the unseen samples. In order to proceed and study
such learning algorithms systematically we need to define the "goal of
learning".

➢ Next session!
