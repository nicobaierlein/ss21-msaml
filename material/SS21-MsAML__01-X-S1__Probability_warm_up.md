# Exercise 01-XS1: Probability warm up [[PDF](SS21-MsAML__01-XS1__Probability_warm_up.pdf)]

As a warm up in probability, prove the theorems

* Expectation of the empirical risk
* Geometric nature of conditional expectation
* Bayes classifier

in the introductory module of the [Statistical framework](SS21-MsAML__01-S1__Statistical_framework.md).
