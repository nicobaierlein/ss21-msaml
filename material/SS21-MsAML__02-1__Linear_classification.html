<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__02-1__Linear_classification</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-01-1-pdf">MsAML-01-1 [<a href="SS21-MsAML__02-1__Linear_classification.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#linear-classification-and-the-perceptron-definition">Linear classification and the Perceptron definition</a>
<ul>
<li><a href="#the-perceptron">The Perceptron</a></li>
<li><a href="#a-more-romantic-introduction">A more romantic introduction</a></li>
</ul></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="linear-classification-and-the-perceptron-definition">Linear classification and the Perceptron definition</h2>
<p>In the last module, we have briefly looked into the general supervised learning setting, for which we observed the need to define the <em>goal of learning</em> more precisely. We shall approach this task in the following modules step by step and from two angles: One with an emphasis on the implementation and performance of particular algorithms and one quite independently of particular implementations but focusing on a statistical perspective of the learning problem.</p>
<p>Our first, fairly large topic will be <em>linear classification</em>. It will give us the chance to cover two traditional machine learning models:</p>
<ul>
<li>the <em>Perceptron</em>;</li>
<li>and the <em>Adaptive Linear Neuron</em> or <em>Adaline</em>.</li>
</ul>
<p>Though simple – and very likely also of because of that –, these models still form the building blocks even for modern applications such as deep learning. Although in general, the mathematical analysis tends to be much harder for non-linear problems, perhaps towards the last third of this lecture you might be astonished which simply tricks already suffice to adapt the implementations of these linear classifiers to also treat non-linear problems. Both models go back to the publications:</p>
<ul>
<li><em>A logical calculus of the ideas immanent in nervous activity.</em> Warren S. McCulloch and Walter Pitts. The Bulletin of Mathematical Biophysics, 5(4):115–133, 1943. URL: <a href="http://link.springer.com/article/10.1007/BF02478259" class="uri">http://link.springer.com/article/10.1007/BF02478259</a>, doi:10.1007/BF02478259</li>
<li><em>The perceptron, a perceiving and recognizing automaton.</em> F. Rosenblatt. Project Para. Cornell Aeronautical Laborator, 1957. URL: <a href="https://blogs.umass.edu/brain-wars/files/2016/03/rosenblatt-1957.pdf" class="uri">https://blogs.umass.edu/brain-wars/files/2016/03/rosenblatt-1957.pdf</a></li>
<li><em>Adapting switching circuits.</em> B. Widrow and M.E. Hoff. IRE WESCON Convention record, pages 96–104, 1960. URL: <a href="http://www-isl.stanford.edu/people/widrow/papers/c1960adaptiveswitching.pdf" class="uri">http://www-isl.stanford.edu/people/widrow/papers/c1960adaptiveswitching.pdf</a></li>
</ul>
<p>This introductory module is inspired by the book:</p>
<ul>
<li><em>Python Machine Learning</em> by Sebastian Raschka mentioned in the course references.</li>
</ul>
<p>To keep things simple, we start with the special case of <em>binary classification</em>, meaning <span class="math display">\[\begin{align}
  \mathcal |\mathcal Y|=2,
\end{align}\]</span> where, as a matter of convenience, we will often use either <span class="math inline">\(\mathcal Y=\{-1,+1\}\)</span> or <span class="math inline">\(\mathcal Y=\{0,1\}\)</span> or as labels.</p>
<p>It is almost a tradition in machine learning to start the first steps in binary classification with the Iris data set that we have already introduced in the exercises:</p>
<figure>
<img src="./01/iris_plot_binary.svg" alt="Binary Iris data." /><figcaption aria-hidden="true">Binary Iris data.</figcaption>
</figure>
<p>Note that the Iris data set originally comprises three flower species. However, for the sake of binary classification let us only distinguish between <em>Iris setosa</em>, encoded by class label <span class="math inline">\(-1\)</span>, and the rest, encoded by class label <span class="math inline">\(+1\)</span>, in order to end up with only two class labels.</p>
<p>Following our notes in module <a href="./SS21-MsAML__01-3__Supervised_learning_setting.html">Supervised learning setting</a>, let us employ the notation <span class="math display">\[\begin{align}
  x \in \mathcal X=\mathbb R^2, 
  \qquad 
  x
  =
  \begin{pmatrix}
    x_1\\
    x_2
  \end{pmatrix}
  \quad
  \begin{array}{l}
    \leftarrow \text{ sepal length}\\
    \leftarrow \text{ sepal width}
  \end{array}
\end{align}\]</span> for the features and the feature space, respectively, and for the label space <span class="math display">\[\begin{align}
  \mathcal Y=\{-1, +1\}.
\end{align}\]</span></p>
<p>Each point <span class="math inline">\(x^{(i)}\in\mathcal X\)</span> in the scatter plot above represents one measurement of a flower specimen and its color denotes its class label <span class="math inline">\(y^{(i)}\in\mathcal Y\)</span>.</p>
<p>As regards our first learning goal, let us attempt to design an algorithm <span class="math inline">\(\mathcal A\)</span> that, by inspection of these measured data tuples <span class="math display">\[\begin{align}
  (x^{(i)},y^{(i)})_{i=1,\ldots,N} \in (\mathcal X\times \mathcal Y)^N,
\end{align}\]</span> finds, or “learns”, a hypothesis <span class="math display">\[\begin{align}
  h:\mathcal X\to\mathcal Y,
\end{align}\]</span> which in turn is then able to also predict class labels <span class="math inline">\(h(x)\in\mathcal Y\)</span> even for previously unseen feature vectors <span class="math inline">\(x\)</span>, e.g., measurements of new flower specimens.</p>
<p>How could the performance of the algorithm be monitored? The Iris data set contains only <span class="math inline">\(M=150\)</span> samples. Clearly, it is not difficult to find some hypothesis <span class="math inline">\(h\)</span> that correctly classifies these data samples without a flaw by simply making sure it fulfills <span class="math display">\[\begin{align}
  \forall i = 1,\ldots,M: \, h(x^{(i)})=y^{(i)},
\end{align}\]</span> and whatever it pleases for other feature vectors. However, in general this would mean that <span class="math inline">\(h\)</span> may not have a good chance to classify unseen feature vectors <span class="math inline">\(x\neq x^{(i)}\)</span> with a better performance that guessing. And it is exactly these new feature vectors that we are interested. The performance of <span class="math inline">\(h\)</span> to correctly classify these unseen features would allow us to understand better whether the learned hypothesis <span class="math inline">\(h\)</span> reflects a general concept behind the assignment of class labels <span class="math inline">\(y\)</span> to given measurements <span class="math inline">\(x\)</span>, i.e., sepal length and width to Iris species in our case, or whether it is only a simple recording of the known training data.</p>
<p>So the tricky part for the algorithm <span class="math inline">\(\mathcal A\)</span> is to pick a hypothesis <span class="math inline">\(h\)</span> that also <em>generalizes</em> well to previously unseen data. It is good to take a minute to fully appreciate the difficulty in this task. As the unseen data feature <span class="math inline">\(x\)</span> is unknown by definition, who is to say what its label is? Consider the following regression problem: How is the sequence of numbers <span class="math display">\[\begin{align}
  2, 4, 8, 16, 32, \ldots 
\end{align}\]</span> to be continued? Well, we could safely say, the next number is given by 42, of course, and also give a reference to the <a href="https://en.wikipedia.org/wiki/The_Hitchhiker%27s_Guide_to_the_Galaxy">classical literature</a>. Why would some people try to convince us that the correct answer is but 64? They might have stumbled over an obvious regularity in the training data <span class="math display">\[\begin{align}
  (1,2), (2, 4), (3, 8), (4,16), (5,32)
\end{align}\]</span> which is broken by assigning of 42 as next number for the previously unseen feature 6. The truth is, of course, that the question is not well-posed, and hence, there is no correct answer without having prior knowledge that points our search to certain types of regularities. If you recognize the severity of this problem, you are in good company. Have a look at “Meno by Socrates” at least starting from this passage:</p>
<blockquote>
<p>Meno: And how will you enquire, Socrates, into that which you do not know? What will you put forth as the subject of enquiry? And if you find what you want, how will you ever know that this is the thing which you did not know?</p>
<p>Socrates: I know, Meno, what you mean; but just see what a tiresome dispute you are introducing. You argue that man cannot enquire either about that which he knows, or about that which he does not know; for if he knows, he has no need to enquire; and if not, he cannot; for he does not know the, very subject about which he is to enquire.”</p>
<p>—excerpt from Meno by Plato, Translation by B. Jowett</p>
</blockquote>
<p>We will leave the general study of learning to the philosophers and only focus on much more simple subfield, namely statistical learning – a new event close to one that was observed with high frequency is likely of the same class. With his paradigm we will already have a good chance to predict that the sun will rise tomorrow, although one day, we might of course miss the low probability asteroid event that may prevent the sun rise.</p>
<p>A prior knowledge, giving meaning to “close, high, and likely”, can then be modeled directly by means of the choice of the algorithm <span class="math inline">\(\mathcal A\)</span> of our supervised learner. And in our statistical analysis we will also see that, without it, beyond the known training data, any supervised learner will on average not perform better than guessing.</p>
<p>Before we get to the task of modeling, let us briefly think about how to measure the generalization performance of such an algorithm <span class="math inline">\(\mathcal A\)</span>. In principle, we encounter the same problem as with prediction of class labels for unseen data as the latter is unknown by definition. Since we may not have an Iris flower garden at hand, we will simulate taking new specimens by presenting the algorithm <span class="math inline">\(\mathcal A\)</span> only a fraction of these known examples for the purpose of training. Let us say, each time we distribute the labels <span class="math inline">\(i=1,\ldots M\)</span> randomly and divide the known data into <em>training data</em> <span class="math display">\[\begin{align}
  (x^{(i)},y^{(i)})_{i=1,\ldots,N}
\end{align}\]</span> and <em>test data</em> <span class="math display">\[\begin{align}
  (x^{(i)},y^{(i)})_{i=N+1,\ldots,M}.
\end{align}\]</span> During training we only present the training data to the algorithm <span class="math inline">\(\mathcal A\)</span> in order to learn the hypothesis <span class="math inline">\(h\)</span>. Afterwards, we may test the performance of <span class="math inline">\(h\)</span> on both the training and test data separately. The former test reflects how accurately <span class="math inline">\(h\)</span> classifies the training data and the latter how it might perform on a subset of potential “unseen” data – having in mind that this subset might not have been selected fairly. This procedure is called <em>cross validation</em>.</p>
<p>In general, we will observe a trade-off between achieving a high classification score of the training data and at the same time a good performance on the classification of the test data. Optimizing the performance on the training data usually drives the found hypotheses into specializing to much on the peculiarities of the particular batch of training data at hand and generalize worse on unseen data, a process that is called overfitting, while gaining performance on the test data may cost us the acceptance of several misclassification, e.g., regarding potential outliers, edge cases, or even intrinsic errors in the hand-annotated training data.</p>
<p>Now that we may appreciate the difficulty in formalizing a learning goal and the importance of the modeling of a priori knowledge, let us have a look at our first model.</p>
<h3 id="the-perceptron">The Perceptron</h3>
<p>Coming back to our original learning task, to find a hypothesis <span class="math inline">\(h\)</span> that allows to distinguish the Iris setosa from all other flowers on the basis of sepal length and with. A first modeling choice can be made by restricting the range of the algorithm <span class="math inline">\(\mathcal A\)</span>.</p>
<p>Looking at the Iris data set, a sensible representation of a hypothesis could be a straight line that best separates the data points according to the classes <span class="math inline">\(+1\)</span> and <span class="math inline">\(-1\)</span>. A good choice would be for example the green straight line:</p>
<figure>
<img src="02/binary_classification_good_choice.png" alt="Good choice for the binary classification." /><figcaption aria-hidden="true">Good choice for the binary classification.</figcaption>
</figure>
<p>A bad choice would obviously be:</p>
<figure>
<img src="02/binary_classification_bad_choice.png" alt="Bad choice for the classification." /><figcaption aria-hidden="true">Bad choice for the classification.</figcaption>
</figure>
<p>This is the a priori knowledge that we want to model. The straight line in <span class="math inline">\(\mathbb R^d\)</span> for <span class="math inline">\(d=2\)</span> in our case (or more general, a hyperplane for any <span class="math inline">\(d\in\mathbb N\)</span>) can be conveniently represented by the normal equation: <span class="math display">\[\begin{align}
0 = \underbrace{w\cdot x}_{=\sum_{j=1}^d w_j x_j}+b
\end{align}\]</span> for normal vector <span class="math inline">\(w\in\mathbb R^d\)</span> and displacement (or bias term) <span class="math inline">\(b\in\mathbb R\)</span>.</p>
<p>Hence, the parametrization of our hypothesis space can be defined as follows: <span class="math display">\[\begin{align}
  \mathcal H
  :=
  \Big\{
    &amp;
    h:\mathbb R^d\to \{-1,+1\}
    \,\big|\, 
    \\
    &amp;
    \exists w\in\mathbb R^d, b\in\mathbb R \, \forall x\in\mathbb R^d:
    h(x)=\text{sign}(w\cdot x+b)
  \Big\}
  \tag{H}
  \label{eq_hypothesis_space}
\end{align}\]</span></p>
<dl>
<dt>👀</dt>
<dd>Implement the scatter plot, of the Iris data set, and for given hypothesis <span class="math inline">\(h\)</span> in <span class="math inline">\(\eqref{eq_hypothesis_space}\)</span>, plot the corresponding straight line and shade the regions of features <span class="math inline">\(x\)</span> that would be assigned label <span class="math inline">\(+1\)</span> and <span class="math inline">\(-1\)</span> with different colors, respectively. Find a good choice of <span class="math inline">\(h\)</span> by hand and let your implementation measure your performance by counting the number of false predictions.
</dd>
</dl>
<p>The restriction to hypotheses <span class="math inline">\(h\)</span>, and therewith the restriction of range of <span class="math inline">\(\mathcal A\)</span>, to hyperplanes is the reason for the term <em>affine linear</em> or simply <em>linear classification</em>. Of course, on data sets which are not separable by such hyperplanes, a perfect classification score is impossible. Either the corresponding errors are still acceptable and one may still consider the underlying learning problem an “almost” linear classification problem, or more general models will have to be used, which we will get to later in this course.</p>
<p>With the parametrization <span class="math inline">\(\eqref{eq_hypothesis_space}\)</span> of the hypothesis space <span class="math inline">\(\mathcal H\)</span> in mind, let us look at a first algorithm <span class="math inline">\(\mathcal A\)</span> that should automatically find a good choice of <span class="math inline">\(w\in\mathbb R^d\)</span> and <span class="math inline">\(b\in\mathbb R\)</span>:</p>
<dl>
<dt>Definition</dt>
<dd><h5 id="perceptron">(Perceptron)</h5>
The realization (as it is actually a random variable when defined as below) of the Perceptron algorithm is a map <span class="math display">\[\begin{align}
\mathcal A:\cup_{N\in\mathbb N} (\mathbb R^d,\{-1,1\})^N\to \mathcal H
  \end{align}\]</span> which is specified by means of the computation steps below:
</dd>
</dl>
<p><strong>Input for <span class="math inline">\(\mathcal A\)</span>:</strong> Training data <span class="math display">\[\begin{align}
  (x^{(i)},y^{(i)})_{i=1,\ldots,N}.
\end{align}\]</span></p>
<p><strong>Step 1:</strong> Initialize <span class="math inline">\(w\in\mathbb R^d\)</span> and <span class="math inline">\(b\in \mathbb R\)</span> with random coefficients which defines the hypothesis <span class="math display">\[\begin{align}
  h_{(w,b)}(x) := \text{sign}(w\cdot x +b).
\end{align}\]</span></p>
<p><strong>Step 2:</strong> Pick a training data sample at random, say <span class="math inline">\((x^{(i)},y^{(i)})\)</span>, and:</p>
<ol type="1">
<li>Compute the prediction <span class="math display">\[\begin{align}
  y = h_{(w,b)}(x^{(i)});
\end{align}\]</span></li>
<li>Compare with <span class="math inline">\(y\)</span> the actual label <span class="math inline">\(y^{(i)}\)</span>:
<ul>
<li>If <span class="math inline">\(y=y^{(i)}\)</span>, and the maximum allowed total number of iterations is not exceeded, go back to <strong>Step 2</strong>.</li>
<li>Else, if <span class="math inline">\(y\neq y^{(i)}\)</span>, we apply the <strong>Update Rule</strong> defined below to adjust our choice for <span class="math inline">\(w,b\)</span> and thereby our hypothesis <span class="math inline">\(h_{(w,b)}\)</span>. Unless the maximum allowed total number of iterations is exceeded, we go back to <strong>Step 2</strong>.</li>
</ul></li>
</ol>
<p><strong>Output of <span class="math inline">\(\mathcal A\)</span>:</strong> The adjusted parameters <span class="math inline">\(w\in\mathbb R^d\)</span> and <span class="math inline">\(b\in\mathbb R\)</span> and therefore a “hopefully” better hypothesis <span class="math display">\[\begin{align}
  h_{(w,b)} \in\mathcal H.
\end{align}\]</span></p>
<p>The most interesting part is of course the following:</p>
<p><strong>Update Rule:</strong> Compute the difference <span class="math display">\[\begin{align}
  \Delta^{(i)} &amp;= y^{(i)} - y \\
  &amp;= y^{(i)} - h_{(w,b)}(x^{(i)})
\end{align}\]</span> and adjust the parameters <span class="math inline">\(w,b\)</span> as follows <span class="math display">\[\begin{align}
  w &amp; \leftarrowtail w^\text{new} := w + \eta \Delta^{(i)} x^{(i)}\\
  b &amp; \leftarrowtail b^\text{new} := b + \eta \Delta^{(i)}\\
\end{align}\]</span> where <span class="math inline">\(\eta\in\mathbb R^+ \backslash \{0\}\)</span> is a fixed, so-called <em>learning rate</em>.</p>
<p>Why does this update rule have a good chance to “improve” the parameter choices for <span class="math inline">\(w,b\)</span> each time a misclassification is detected?</p>
<p>In view of a misclassification, two cases may occur:</p>
<ul>
<li><span class="math inline">\(\Delta^{(i)}=+2\)</span>: This means that the actual label is given by <span class="math inline">\(y^{(i)}=+1\)</span> and the prediction was <span class="math inline">\(h_{(w,b)}(x^{(i)})=-1\)</span>, and hence, <span class="math display">\[\begin{align}
    w\cdot x^{(i)}+b &lt; 0.
  \end{align}\]</span> After carrying out the update rule, the updated normal vector <span class="math display">\[\begin{align}
    w^\text{new} &amp;= w + \eta \Delta^{(i)} x^{(i)}\\
    &amp;= w + 2\eta x^{(i)}
  \end{align}\]</span> and likewise the displacement parameter <span class="math display">\[\begin{align}
    b^\text{new} 
    &amp;= b + \eta\Delta^{(i)} \\
    &amp;= b + 2\eta
  \end{align}\]</span> imply <span class="math display">\[\begin{align}
    &amp; \left(w^\text{new}\cdot x^{(i)}+b^\text{new}\right)
    -\left(w\cdot x^{(i)}+b\right)\\
    =&amp;
    \underbrace{\eta}_{&gt;0}
    \,
    \underbrace{\Delta^{(i)}}_{&gt;0}
    \,
    \underbrace{(x^{(i)})^2}_{\geq 0}
    +
    \underbrace{\eta}_{&gt;0}
    \,
    \underbrace{\Delta^{(i)}}_{&gt;0}\\
    &gt;&amp;  0
  \end{align}\]</span> so that <span class="math inline">\(h_{w^\text{new},b^\text{new}}(x^{(i)}) = \text{sign}\left(w^\text{new}\cdot x^{(i)}+b^\text{new}\right)\)</span> has a better chance to evaluate to <span class="math inline">\(+1\)</span> instead of <span class="math inline">\(h_{w,b}(x^{(i)})=\text{sign}\left(w\cdot x^{(i)}+b\right)=-1\)</span>.</li>
<li><span class="math inline">\(\Delta^{(i)}=-2\)</span>: 👀 This case is analogous, try on your own.</li>
</ul>
<dl>
<dt>👀</dt>
<dd>How will <span class="math inline">\(\eta\)</span> affect the learning?
</dd>
</dl>
<p>In summary, we have set up an algorithm and gave a heuristic argument why, after sufficiently many iterations, it might provide a good choice for a hypothesis <span class="math inline">\(h\)</span> to predict the class labels <span class="math inline">\(y=h(x)\in\mathcal Y\)</span> for any new measurements <span class="math inline">\(x\in \mathcal X\)</span>. In other words, a first model of supervised machine learning.</p>
<p>Our next steps are:</p>
<ul>
<li>A Python implementation;</li>
<li>An analysis of its prediction performances on the Iris data set;</li>
<li>A proof of convergence of the iteration of the Perceptron algorithm in finite many steps should the training data be linearly separable. ➢ Next session!</li>
</ul>
<h3 id="a-more-romantic-introduction">A more romantic introduction</h3>
<p>The more romantic picture behind the Perceptron model is to consider it as a mathematical caricature of a neuron cell:</p>
<figure>
<img src="02/neuron.png" alt="A caricature of a neuron (Source)." /><figcaption aria-hidden="true">A caricature of a neuron (<a href="https://en.wikipedia.org/wiki/Neuron">Source</a>).</figcaption>
</figure>
<ul>
<li>The feature vector <span class="math inline">\(\mathbb R^d\)</span> provides the input stimulus at the dendrites;</li>
<li>The coefficients <span class="math inline">\(w_j\)</span> in the weight vector <span class="math inline">\(w\in\mathbb R^d\)</span> model the sensitivity of the dendrites;</li>
<li>The displacement term <span class="math inline">\(b\)</span> models an inherent bias on the neuron;</li>
<li>The <em>activation function</em> <span class="math inline">\(\text{sign}(\cdot)\)</span> maps the <em>activation</em> <span class="math display">\[\begin{align}
    z(x)=w\cdot x+b
  \end{align}\]</span> of the axon to a “clean” signal <span class="math display">\[\begin{align}
    y=\text{sign}(z(x))=h(x) \in \{-1,+1\}.
  \end{align}\]</span></li>
</ul>
<p>It goes without saying that this is a very simple model of a neuron, nevertheless as we will see, with some minor modifications (leading to the Adaline neuron), a very versatile and effective building block of artificial neural networks employed even in modern deep learning techniques. One often calls the Perceptron a “first generation” and the Adaline a “second generation” artificial neuron. Despite their effectiveness, they do not allow to model some known phenomena of biological neurons. One obvious limitation of the model is that information can only be encoded by “amplitude modulation” in the activation function. Biological neurons, however, seem to encode information also in the “frequency modulation” of their firing rates. The latter may have physiological explanations but even without them one can imagine a frequency modulation may be more robust in noisy environments.</p>
<p>Be that as it may, by now there is a “third generation”, the so-called <a href="https://en.wikipedia.org/wiki/Spiking_neural_network"><em>spiking neurons</em></a> that provide the first more sophisticated mathematical models to model properties such as amplitudes, frequencies, delays, thresholds in the activation functions. Unfortunately, yet no general and efficient method of training is known and the computational efforts is much more costly. However, that is likely to change as many efforts are currently invested. For example, <a href="https://www.intel.com/content/www/us/en/research/neuromorphic-computing.html">Intel’s Loihi chip</a>, features a programmable hardware implementation network of 130.000 such spiking neurons.</p>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
