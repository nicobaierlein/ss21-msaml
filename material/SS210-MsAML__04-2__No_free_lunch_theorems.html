<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS210-MsAML__04-2__No_free_lunch_theorems</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-04-2-pdf">MsAML-04-2 [<a href="SS210-MsAML__04-2__No_free_lunch_theorems.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#no-free-lunch-theorems">No free lunch theorems</a></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="no-free-lunch-theorems">No free lunch theorems</h2>
<p>As discussed, the generalization performance of our models will depend on a priori knowledge. With the following theorem, we will get a first impression to what an extend this is the case.</p>
<p>Let us consider the following statistical learning setting:</p>
<ul>
<li>finite feature and label spaces <span class="math inline">\(\mathcal X,\mathcal Y\)</span>;</li>
<li>given training data <span class="math inline">\(s\in(\mathcal X\times\mathcal Y)^N\)</span>, our favorite algorithm <span class="math inline">\(\mathcal A\)</span> always succeeds to find a hypothesis <span class="math inline">\(h_s=\mathcal A(s)\)</span>;</li>
<li>we regard deterministic learning tasks only, for which there exists a so-called <em>concept</em> <span class="math inline">\(c:\mathcal X\to\mathcal Y\)</span> that, for any feature vector <span class="math inline">\(x\in\mathcal X\)</span>, deterministically assigns the corresponding actual label <span class="math inline">\(y=c(x)\in\mathcal Y\)</span>;</li>
<li>as error probability (or generalization error) of a learned hypothesis <span class="math inline">\(h_s\)</span> we regard a random variable of features <span class="math inline">\(X\)</span> uniformly distributed over <span class="math inline">\(\mathcal X\)</span> and define <span class="math display">\[\begin{align}
  P_X(h_s(X)\neq c(X));
\end{align}\]</span></li>
<li>we represent the training data <span class="math inline">\(S=(X^{(i)},Y^{(i)})_{i=1,\ldots,N}\)</span> at hand by random feature tuples <span class="math inline">\(X^N\equiv (X^{(1)},\ldots,X^{(N)})\)</span> uniformly distributed over <span class="math inline">\(\mathcal X^N\)</span> for given <span class="math inline">\(N\leq |\mathcal X\)</span> and corresponding labels <span class="math inline">\(Y^{(i)}=c(X^{(i)})\)</span> determined by the underlying concept of the learning task;</li>
<li>finally, as we want to regard not only one learning task but an average over all, we introduce another random variable <span class="math inline">\(C\)</span> which is uniformly distributed over the finite space of possible concepts <span class="math inline">\(\mathcal Y^\mathcal{X}\)</span>.</li>
</ul>
<dl>
<dt>Theorem</dt>
<dd><h5 id="no-free-lunch-theorem">(No free lunch theorem)</h5>
For any such algorithm in this setting, the bounds <span class="math display">\[\begin{align}
&amp;\frac{N}{|\mathcal X|\,|\mathcal Y|}+\left(1-\frac{1}{|\mathcal Y|}\right)
\\
&amp;\qquad\qquad
\geq 
E_C E_{X^N} P_X\left( h_S(X)\neq c(X)\right)
\\
&amp;\qquad\qquad \qquad \geq 
\left(1-\frac{N}{|\mathcal X|}\right)
\left(1-\frac{1}{|\mathcal Y|}\right)
  \end{align}\]</span> hold true.
</dd>
</dl>
<p>Before we turn to the proof, let us review the meaning of the term sandwiched between these upper and lower bounds. It is the average <span class="math inline">\(E_C\)</span> with respect to uniformly chosen concepts <span class="math inline">\(C\)</span> in <span class="math inline">\(\mathcal Y^\mathcal{X}\)</span> of the average <span class="math inline">\(E_{X^N}\)</span> with respect to uniformly chosen training data features <span class="math inline">\(X^N\)</span> in <span class="math inline">\(\mathcal X^N\)</span> of the error probability for the learned hypothesis <span class="math inline">\(h_S\)</span> and underlying concept <span class="math inline">\(C\)</span>.</p>
<p>This quantity measures how well our favorite algorithm <span class="math display">\[\begin{align}
  \mathcal A:(\mathcal X,\mathcal Y)^N &amp;\to\mathcal Y^\mathcal{X}, \\
  s &amp;\mapsto \mathcal A(s)\equiv h_s
\end{align}\]</span> generalizes on average with respect to all possible deterministic learning tasks in finding the this underlying concept <span class="math inline">\(C\)</span> under fairly chosen training data.</p>
<p><strong>Proof:</strong> We begin by defining the set <span class="math display">\[\begin{align}
  \mathcal X_{X^N}:=\left\{ X^{(i)} \,|\, i=1,\ldots, N\right\}\subset \mathcal X,
\end{align}\]</span> i.e., the subset of feature vectors appearing in the training data. In case, we find only <span class="math inline">\(|\mathcal X_{X^N}|&lt;N\)</span> many, we add further elements of <span class="math inline">\(\mathcal X\)</span> until <span class="math inline">\(|\mathcal X_{X^N}|=N\)</span>.</p>
<p>Let us write the innermost probability as an expectation: <span class="math display">\[\begin{align}
  &amp;E_C E_{X^N} P_X\left(h_S(X)\neq C(X)\right)\\
  &amp;=
  E_C E_{X^N} \frac{1}{|\mathcal X|}\sum_{x\in\mathcal X} 1_{h_S(x)\neq C(x)}
  \\
  &amp;=: \fbox{1}.
\end{align}\]</span> As the characteristic function of the right-hand side is non-negative, this quantity can be estimated from below by replacing <span class="math inline">\(\mathcal X\)</span> in the summation by the complement of the subset <span class="math inline">\(\mathcal X_{X^N}\)</span>: <span class="math display">\[\begin{align}
  \fbox{1} \geq 
  &amp;\frac{1}{|\mathcal X|} E_C E_{X^N} \sum_{x\notin\mathcal X_S} 1_{h_S(x)\neq C(x)} 
  \\
  &amp;=: \fbox{2}.
\end{align}\]</span> Note that, while we can move the constant <span class="math inline">\(\frac{1}{|\mathcal X|}\)</span> to the front by linearity of the expectation, the domain of the summation itself depends on the random training data features <span class="math inline">\(X^N\)</span>.</p>
<p>However, we observe that, while for each <span class="math inline">\(x\in\mathcal X_{X^N}\)</span>, the training data <span class="math inline">\(S\)</span>, being of function of <span class="math inline">\(X^N\)</span>, and the value of <span class="math inline">\(C(x)\)</span> are generally dependent, for <span class="math inline">\(x\notin \mathcal X_{X^N}\)</span> all values of <span class="math inline">\(C(x)\)</span> in <span class="math inline">\(\mathcal Y\)</span> are independent of <span class="math inline">\(S\)</span>, and thus, equally likely. In turn, for concepts <span class="math inline">\(C\)</span> that do not change the training data <span class="math inline">\(S\)</span> and therewidth <span class="math inline">\(h_S\)</span>, the event <span class="math inline">\(h_S(x)\neq C(x)\)</span> for <span class="math inline">\(x\notin \mathcal X_S\)</span> amounts to picking one value out of <span class="math inline">\(|\mathcal Y|\)</span> many, an event of probability of <span class="math inline">\(\left(1-\frac{1}{|\mathcal Y|}\right)\)</span>. The remaining sum has <span class="math inline">\((|\mathcal X|-N)\)</span> summands so that we get: <span class="math display">\[\begin{align}
  \fbox{2} =
  &amp;\frac{1}{|\mathcal X|} (|\mathcal X|-N) \left(1-\frac{1}{|\mathcal Y|}\right)
  \tag{T2}
  \label{eq_term2}
  \\
  &amp;= (1-\frac{N}{|\mathcal X|})\left(1-\frac{1}{|\mathcal Y|}\right),
\end{align}\]</span> which proves the lower bound.</p>
<p>The upper bound can be seen as follows: <span class="math display">\[\begin{align}
  &amp;E_C E_{X^N} P_X\left(h_S(X)\neq C(X)\right)
  \\
  &amp;=E_C  E_{X^N} \frac{1}{|\mathcal X|}\left(
    \sum_{x\notin\mathcal X_{X^N}}1_{h_S(X)\neq C(X)}
    + \sum_{x\in\mathcal X_{X^N}}1_{h_S(X)\neq C(X)}
  \right)
  \\
  &amp;\leq E_C E_{X^N} \frac{1}{|\mathcal X|}\left(
    \sum_{x\notin\mathcal X_{X^N}}1_{h_S(X)\neq C(X)}
    + N
  \right)
  \\
  &amp; =: \fbox{3},
\end{align}\]</span> where we can again apply equality <span class="math inline">\(\eqref{eq_term2}\)</span> to find <span class="math display">\[\begin{align}
  \fbox{3} = \left(1-\frac{1}{|\mathcal Y|}\right) + \frac{N}{|\mathcal X|\,|\mathcal Y|}.
\end{align}\]</span> This proves the upper bound and concludes the proof.</p>
<p><span class="math inline">\(\square\)</span></p>
<p>What does this theorem entail? The factor <span class="math inline">\(\left(1-\frac{1}{|\mathcal Y|}\right)\)</span> is the probability of randomly guessing a label out of <span class="math inline">\(|\mathcal Y|\)</span> possibilities with uniform distribution. Now the lower bound of the theorem entails that, provided <span class="math inline">\(N\)</span> training data samples were inspected by the algorithm during training, on average, there is only a factor <span class="math inline">\(\left(1-\frac{N}{|\mathcal X|}\right)\)</span> left for room of improvement in the error probability as compared to randomly guessing. This room of improvement is entirely due to the possibility of simply recording the inspected training data during training.</p>
<p>In applications, while the amount of class labels <span class="math inline">\(|\mathcal Y|\)</span> is usually small, e.g., as low as two for binary classification, <span class="math inline">\(|\mathcal X|\)</span> is typically very large, even, when compared to the amount of training data samples <span class="math inline">\(N\)</span>, so that the fraction <span class="math inline">\(\frac{N}{|\mathcal X|}\)</span> is very small. Observing the upper and lower bounds in the limit of <span class="math inline">\(\frac{N}{|\mathcal X|}\to 0\)</span>, we find that any algorithm will on average only perform a little bit better than randomly guessing a class label independently of the presented feature vector.</p>
<p>In other words, although our favorite algorithm may do very well on a specific learning tasks, i.e., in the above context of learning a concept <span class="math inline">\(c\in\mathcal Y^\mathcal{X}\)</span>, but it is bound to do very poorly on another one.</p>
<p>There are much more sophisticated version of such no free lunch theorems, in particular, weakening the uniformity assumption in the distribution of concepts as not all concepts may be equally important but the entailed spirit is the same.</p>
<p>On average, no supervised learner is better than another as regards unseen test data. The only way to boost the generalization performance is to provide a priori knowledge for the learning task, as for example the regularity assumption in finding a continuation of sequence <span class="math inline">\(2, 4, 8, 16, 32, \ldots\)</span> or the restriction to separating hyperplanes for linearly separable training data, etc. In the rest of this course, we will discuss several ways to incorporate a priori knowledge into our model definitions. In order to do so, there are two main routes, one in putting a priori knowledge into the distribution of training data <span class="math inline">\(P\)</span>, e.g., with the help of a parametric model of <span class="math inline">\(P\)</span>, and one in putting restricting of the range of hypothesis selected by the resulting algorithm <span class="math inline">\(\mathcal A\)</span>. This latter approach is to some extend independent of the unknown distribution <span class="math inline">\(p\)</span> of the training data and this will be the main one studied in this course.</p>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
