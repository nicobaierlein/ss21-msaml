# Mathematic(al Statistic)s and Applications of Machine Learning [[PDF](index.pdf)]

1. [Table of contents](#table-of-contents)
2. [Weekly course material](#weekly-course-material)
   * [Week of April 12: Lecture 01](#week-of-april-12:-lecture-01)
   * [Week of April 19: Lecture 02](#week-of-april-19:-lecture-02)
3. [Course material overviews](#course-material-overviews)
   * [Overview of MAML topics](#overview-of-maml-topics)
   * [Overview of MSAML topics](#overview-of-msaml-topics)

Back to the [GitLab course site](https://gitlab.com/dirk-deckert-lmu/ss21-msaml).


## Weekly course material 


### Week of April 12: Lecture 01

Organization:

* [Get-to-know session](SS21-MsAML__01-1__Get_to_know_session.pdf) [[Video](https://cast.itunes.uni-muenchen.de/clips/hcrhoalPU3/vod/online.html)]

Lectures notes:

* [Introduction and overview](SS21-MsAML__01-2__Introduction_and_overview.md)
* [Supervised learning setting](SS21-MsAML__01-3__Supervised_learning_setting.md)
* \(S\) [Statistical framework](SS21-MsAML__01-S1__Statistical_framework.md)

Exercises and solutions:

|       | Exercise                                                                                                       | Discussion | Solution                                                                                                                                                                                        |
|-------|----------------------------------------------------------------------------------------------------------------|------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 01-0  | [Python warmup](SS21-MsAML__01-X-0__Python_warmup.ipynb)                                                       | #5         | included                                                                                                                                                                                        |
| 01-1  | [Set up a Python development environment](SS21-MsAML__01-X-1__Set_up_a_Python_development_environment.md)      | #6         | included                                                                                                                                                                                        |
| 01-2  | [First steps with Numpy](SS21_MsAML__01-X-2__First_steps_with_numpy.ipynb)                                     | #7         | included                                                                                                                                                                                        |
| 01-3  | [Plotting and obtaining data](SS21_MsAML__01-X-3__Plotting_and_obtaining_data.ipynb)                           | #8         | included                                                                                                                                                                                        |
| 01-M1 | [Histograms from Covid-19 time series data](SS21_MsAML__01-X-M1__Histograms_from_Covid-19_time_series_data.md) | #9         | [IPython Notebook](SS21_MsAML__01-X-M1__Histograms_from_Covid-19_time_series_data.ipynb)                                                                                                        |
| 01-S1 | [Probability warm up](SS21-MsAML__01-X-S1__Probability_warm_up.md)                                             | #10        | [1](SS21-MsAML__01-X-S1__Empirical_Risk_Expectation_SOLUTION.pdf), [2](SS21-MsAML__01-X-S1__Quadratic_risk_decomposition_SOLUTION.pdf), [3](SS21-MsAML__01-X-S1__Bayes_classifier_SOLUTION.pdf) |


### Week of April 19: Lecture 02

Lectures notes:

* [Linear classification and the perceptron definition](SS21-MsAML__02-1__Linear_classification.md)
* \(S\) [Error decomposition](SS21-MsAML__02-S1__Error_decomposition.md)

Exercises and solutions:

|       | Exercise                                                                                                                             | Discussion | Solution |
|-------|--------------------------------------------------------------------------------------------------------------------------------------|------------|----------|
| 02-1  | [Limits of linear classification](SS21-MsAML__02-X1__Limits_of_linear_classification.md)                                             | #16        |  [Solution](SS21-MsAML__02-X1__Limits_of_linear_classification_SOL.pdf)        |
| 02-2  | [Preparation of Python tools for linear classification](SS21-MsAML__02-X2__Preparation_of_Python_tools_for_linear_classification.md) | #17        |  [Solution](SS21-MsAML__02-X2__Preparation_of_Python_tools_for_linear_classification_SOL.ipynb)        |
| 02-3  | [Perceptron_implementation](SS21-MsAML__02-X3__Perceptron_implementation.md)                                                         | #18        | [Solution](SS21-MsAML__02-X3__Perceptron_implementation_SOL.ipynb)        |
| 02-S1 | [Linear regression model](SS21-MsAML__02-XS1__Linear_regression_model.md)                                                            | #19        | [Solution](SS21-MsAML__02-XS1__Linear_regression_model_SOL.pdf)         |


### Week of April 26: Lecture 03

Lectures notes:

* [Perceptron convergence](SS21-MsAML__03-1__Perceptron_convergence.md)
* [Adaline definition](SS21-MsAML__03-2__Adaline_definition.md)
* \(S\) [A first simple learning setting](SS21-MsAML__03-S1__A_simple_learning_setting.md)

Exercises and solutions:

|       | Exercise                                                                                     | Discussion | Solution |
|-------|----------------------------------------------------------------------------------------------|------------|----------|
| 03-1  | [Perceptron convergence](SS21-MsAML__03-X1__Perceptron_convergence.md)                       | #21        | [Solution](SS21-MsAML__03-X1__Perceptron_convergence_SOL.pdf)         |
| 03-2  | [Adaline learning rate](SS21-MsAML__03-X2__Adaline_learning_rate.md)                         | #22        |  [Solution](SS21-MsAML__03-X2__Adaline_learning_rate_SOL.pdf), [Jupyter Notebook](SS21-MsAML__03-X2__Adaline_learning_rate_SOL_1.ipynb)        |
| 03-S1 | [A first simple learning setting](SS21-MsAML__03-S1__A_simple_learning_setting.md) Tasks 1-5 | #23        | included |
| 03-S2 | [Gradient descent with least squares](SS21-MsAML__03-XS2__Gradient_descent_least_squares.md) | #24        | [Solution](SS21-MsAML__03-XS2__Gradient_descent_least_squares_SOL.pdf)         |


### Week of May 3: Lecture 04

Lectures notes:

* [Perceptron-Adaline comparison](SS210-MsAML__04-1__Perceptron-Adaline_comparison.md)
* [No free lunch theorems](SS210-MsAML__04-2__No_free_lunch_theorems.md)

Exercises and solutions:

|  | Exercise | Discussion | Solution |
|--|----------|------------|----------|
|  |          |            |          |



## Course material overviews 


### Overview of MAML topics 

* [Introduction and overview](SS21-MsAML__01-2__Introduction_and_overview.md) 
* [Supervised learning setting](SS21-MsAML__01-3__Supervised_learning_setting.md)
* [Linear classification and the perceptron definition](SS21-MsAML__02-1__Linear_classification.md)
  * [Perceptron convergence](SS21-MsAML__03-1__Perceptron_convergence.md)
  * [Adaline definition](SS21-MsAML__03-2__Adaline_definition.md)
  * [Perceptron-Adaline comparison](SS210-MsAML__04-1__Perceptron-Adaline_comparison.md)
* [No free lunch theorems](SS210-MsAML__04-2__No_free_lunch_theorems.md)


### Overview of MSAML topics 

* \(S\) [Statistical framework](SS21-MsAML__01-S1__Statistical_framework.md)
* \(S\) [Error decomposition](SS21-MsAML__02-S1__Error_decomposition.md)
* \(S\) [A first simple learning setting](SS21-MsAML__03-S1__A_simple_learning_setting.md)
