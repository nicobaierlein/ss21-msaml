# MsAML-01-1 [[PDF](SS21-MsAML__02-1__Linear_classification.pdf)]

1. [Linear classification and the Perceptron definition](#linear-classification-and-the-perceptron-definition)
   * [The Perceptron](#the-perceptron)
   * [A more romantic introduction](#a-more-romantic-introduction)

Back to [index](index.md).


## Linear classification and the Perceptron definition

In the last module, we have briefly looked into the general supervised learning
setting, for which we observed the need to define the *goal of learning* more
precisely. We shall approach this task in the following modules step by step
and from two angles: One with an emphasis on the implementation and performance
of particular algorithms and one quite independently of particular
implementations but focusing on a statistical perspective of the learning
problem.

Our first, fairly large topic will be *linear classification*. It will give us
the chance to cover two traditional machine learning models:

* the *Perceptron*;
* and the *Adaptive Linear Neuron* or *Adaline*.

Though simple -- and very likely also of because of that --, these models still
form the building blocks even for modern applications such as deep learning.
Although in general, the mathematical analysis tends to be much harder for
non-linear problems, perhaps towards the last third of this lecture you might 
be astonished which simply tricks already suffice to adapt the implementations
of these linear classifiers to also treat non-linear problems. Both models go back
to the publications:

* *A logical calculus of the ideas immanent in nervous activity.* Warren S. McCulloch and Walter Pitts. The Bulletin of Mathematical Biophysics, 5(4):115–133, 1943. URL: <http://link.springer.com/article/10.1007/BF02478259>, doi:10.1007/BF02478259
* *The perceptron, a perceiving and recognizing automaton.* F. Rosenblatt. Project Para. Cornell Aeronautical Laborator, 1957. URL: <https://blogs.umass.edu/brain-wars/files/2016/03/rosenblatt-1957.pdf>
* *Adapting switching circuits.* B. Widrow and M.E. Hoff. IRE WESCON Convention record, pages 96–104, 1960. URL: <http://www-isl.stanford.edu/people/widrow/papers/c1960adaptiveswitching.pdf>

This introductory module is inspired by the book:

* *Python Machine Learning* by Sebastian Raschka mentioned in the course references.

To keep things simple, we start with the special case of *binary classification*, meaning
\begin{align}
  \mathcal |\mathcal Y|=2,
\end{align}
where, as a matter of convenience, we will often use either $\mathcal
Y=\{-1,+1\}$ or $\mathcal Y=\{0,1\}$ or as labels.

It is almost a tradition in machine learning to start the first steps in binary
classification with the Iris data set that we have already introduced in the
exercises:

![Binary Iris data.](./01/iris_plot_binary.svg)

Note that the Iris data set originally comprises three flower species.
However, for the sake of binary classification let us only distinguish between
*Iris setosa*, encoded by class label $-1$, and the rest, encoded by class
label $+1$, in order to end up with only two class labels.

Following our notes in module [Supervised learning setting](./SS21-MsAML__01-3__Supervised_learning_setting.md), let us employ the notation
\begin{align}
  x \in \mathcal X=\mathbb R^2, 
  \qquad 
  x
  =
  \begin{pmatrix}
    x_1\\
    x_2
  \end{pmatrix}
  \quad
  \begin{array}{l}
    \leftarrow \text{ sepal length}\\
    \leftarrow \text{ sepal width}
  \end{array}
\end{align}
for the features and the feature space, respectively, and for the label space
\begin{align}
  \mathcal Y=\{-1, +1\}.
\end{align}

Each point $x^{(i)}\in\mathcal X$ in the scatter plot above represents one
measurement of a flower specimen and its color denotes its class label
$y^{(i)}\in\mathcal Y$.

As regards our first learning goal, let us attempt to design an algorithm
$\mathcal A$ that, by inspection of these measured data tuples
\begin{align}
  (x^{(i)},y^{(i)})_{i=1,\ldots,N} \in (\mathcal X\times \mathcal Y)^N,
\end{align}
finds, or "learns", a hypothesis 
\begin{align}
  h:\mathcal X\to\mathcal Y,
\end{align}
which in turn is then able to also predict class labels $h(x)\in\mathcal Y$ even
for previously unseen feature vectors $x$, e.g., measurements of new flower
specimens.

How could the performance of the algorithm be monitored? The Iris data set
contains only $M=150$ samples. Clearly, it is not difficult to find some
hypothesis $h$ that correctly classifies these data samples without a flaw by
simply making sure it fulfills
\begin{align}
  \forall i = 1,\ldots,M: \, h(x^{(i)})=y^{(i)},
\end{align}
and whatever it pleases for other feature vectors. However, in general this
would mean that $h$ may not have a good chance to classify unseen feature
vectors $x\neq x^{(i)}$ with a better performance that guessing. And it is
exactly these new feature vectors that we are interested. The performance of
$h$ to correctly classify these unseen features would allow us to understand
better whether the learned hypothesis $h$ reflects a general concept behind the
assignment of class labels $y$ to given measurements $x$, i.e., sepal length
and width to Iris species in our case, or whether it is only a simple recording
of the known training data.

So the tricky part for the algorithm $\mathcal A$ is to pick a hypothesis $h$
that also *generalizes* well to previously unseen data. It is good to take a
minute to fully appreciate the difficulty in this task. As the unseen data
feature $x$ is unknown by definition, who is to say what its label is? Consider
the following regression problem: How is the sequence of numbers
\begin{align}
  2, 4, 8, 16, 32, \ldots 
\end{align}
to be continued? Well, we could safely say, the next number is given by 42, of
course, and also give a reference to the [classical
literature](https://en.wikipedia.org/wiki/The_Hitchhiker%27s_Guide_to_the_Galaxy).
Why would some people try to convince us that the correct answer is but 64?
They might have stumbled over an obvious regularity in the training data 
\begin{align}
  (1,2), (2, 4), (3, 8), (4,16), (5,32)
\end{align}
which is broken by assigning of 42 as next number for the previously unseen
feature 6. The truth is, of course, that the question is not well-posed, and
hence, there is no correct answer without having prior knowledge that points
our search to certain types of regularities. If you recognize the severity of
this problem, you are in good company. Have a look at "Meno by Socrates" at
least starting from this passage:

> Meno: And how will you enquire, Socrates, into that which you do not know?
> What will you put forth as the subject of enquiry? And if you find what you
> want, how will you ever know that this is the thing which you did not know?
> 
> Socrates: I know, Meno, what you mean; but just see what a tiresome dispute
> you are introducing. You argue that man cannot enquire either about that
> which he knows, or about that which he does not know; for if he knows, he has
> no need to enquire; and if not, he cannot; for he does not know the, very
> subject about which he is to enquire.”
> 
> —excerpt from Meno by Plato, Translation by B. Jowett

We will leave the general study of learning to the philosophers and only focus
on much more simple subfield, namely statistical learning -- a new event close
to one that was observed with high frequency is likely of the same class. With
his paradigm we will already have a good chance to predict that the sun will
rise tomorrow, although one day, we might of course miss the low probability
asteroid event that may prevent the sun rise. 

A prior knowledge, giving meaning to "close, high, and likely", can then be
modeled directly by means of the choice of the algorithm $\mathcal A$ of our
supervised learner. And in our statistical analysis we will also see that,
without it, beyond the known training data, any supervised learner will on
average not perform better than guessing.

Before we get to the task of modeling, let us briefly think about how to
measure the generalization performance of such an algorithm $\mathcal A$. In
principle, we encounter the same problem as with prediction of class labels for
unseen data as the latter is unknown by definition. Since we may not have
an Iris flower garden at hand, we will simulate taking new specimens by
presenting the algorithm $\mathcal A$ only a fraction of these known examples
for the purpose of training. Let us say, each time we distribute the labels
$i=1,\ldots M$ randomly and divide the known data into *training data* 
\begin{align}
  (x^{(i)},y^{(i)})_{i=1,\ldots,N}
\end{align}
and *test data*
\begin{align}
  (x^{(i)},y^{(i)})_{i=N+1,\ldots,M}.
\end{align}
During training we only present the training data to the algorithm $\mathcal A$
in order to learn the hypothesis $h$. Afterwards, we may test the performance
of $h$ on both the training and test data separately. The former test reflects
how accurately $h$ classifies the training data and the latter how it
might perform on a subset of potential "unseen" data -- having in mind that
this subset might not have been selected fairly. This procedure is called
*cross validation*.

In general, we will observe a trade-off between achieving a high classification
score of the training data and at the same time a good performance on the
classification of the test data. Optimizing the performance on the training
data usually drives the found hypotheses into specializing to much on the
peculiarities of the particular batch of training data at hand and generalize
worse on unseen data, a process that is called overfitting, while gaining
performance on the test data may cost us the acceptance of several
misclassification, e.g., regarding potential outliers, edge cases, or even
intrinsic errors in the hand-annotated training data.

Now that we may appreciate the difficulty in formalizing a learning goal and
the importance of the modeling of a priori knowledge, let us have a look at our
first model.


### The Perceptron

Coming back to our original learning task, to find a hypothesis $h$ that allows
to distinguish the Iris setosa from all other flowers on the basis of sepal
length and with. A first modeling choice can be made by restricting the range
of the algorithm $\mathcal A$.

Looking at the Iris data set, a sensible representation of a hypothesis could
be a straight line that best separates the data points according to the classes
$+1$ and  $-1$. A good choice would be for example the green straight line:

![Good choice for the binary classification.](02/binary_classification_good_choice.png)

A bad choice would obviously be:

![Bad choice for the classification.](02/binary_classification_bad_choice.png)

This is the a priori knowledge that we want to model. The straight line in
$\mathbb R^d$ for $d=2$ in our case (or more general, a hyperplane for any
$d\in\mathbb N$) can be conveniently represented by the normal equation:
\begin{align}
0 = \underbrace{w\cdot x}_{=\sum_{j=1}^d w_j x_j}+b
\end{align}
for normal vector $w\in\mathbb R^d$ and displacement (or bias term)
$b\in\mathbb R$.

Hence, the parametrization of our hypothesis space can be defined as follows:
\begin{align}
  \mathcal H
  :=
  \Big\{
    &
    h:\mathbb R^d\to \{-1,+1\}
    \,\big|\, 
    \\
    &
    \exists w\in\mathbb R^d, b\in\mathbb R \, \forall x\in\mathbb R^d:
    h(x)=\text{sign}(w\cdot x+b)
  \Big\}
  \tag{H}
  \label{eq_hypothesis_space}
\end{align}

👀
: Implement the scatter plot, of the Iris data set, and for given hypothesis $h$
  in $\eqref{eq_hypothesis_space}$, plot the corresponding straight line and
  shade the regions of features $x$ that would be assigned label $+1$ and $-1$
  with different colors, respectively. Find a good choice of $h$ by hand and
  let your implementation measure your performance by counting the number of
  false predictions.

The restriction to hypotheses $h$, and therewith the restriction of range of
$\mathcal A$, to hyperplanes is the reason for the term *affine linear* or
simply *linear classification*. Of course, on data sets which are not separable
by such hyperplanes, a perfect classification score is impossible. Either the
corresponding errors are still acceptable and one may still consider the
underlying learning problem an "almost" linear classification problem, or more
general models will have to be used, which we will get to later in this course.

With the parametrization $\eqref{eq_hypothesis_space}$ of the hypothesis space
$\mathcal H$ in mind, let us look at a first algorithm $\mathcal A$ that should
automatically find a good choice of $w\in\mathbb R^d$ and $b\in\mathbb R$:

Definition 
: ##### (Perceptron)
  The realization (as it is actually a random variable when defined as below) of the Perceptron
  algorithm is a map 
  \begin{align}
    \mathcal A:\cup_{N\in\mathbb N} (\mathbb R^d,\{-1,1\})^N\to \mathcal H
  \end{align}
  which is specified by means of the computation steps below:

**Input for $\mathcal A$:** Training data
\begin{align}
  (x^{(i)},y^{(i)})_{i=1,\ldots,N}.
\end{align}

**Step 1:** Initialize $w\in\mathbb R^d$ and $b\in \mathbb R$ with random
coefficients which defines the hypothesis
\begin{align}
  h_{(w,b)}(x) := \text{sign}(w\cdot x +b).
\end{align}

**Step 2:** Pick a training data sample at random, say $(x^{(i)},y^{(i)})$,
and:

1. Compute the prediction
\begin{align}
  y = h_{(w,b)}(x^{(i)});
\end{align}
2. Compare with $y$ the actual label $y^{(i)}$:
   * If $y=y^{(i)}$, and the maximum allowed total number of iterations is
       not exceeded, go back to **Step 2**.
   * Else, if $y\neq y^{(i)}$, we apply the **Update Rule** defined below
       to adjust our choice for $w,b$ and thereby our hypothesis $h_{(w,b)}$.
       Unless the maximum allowed total number of iterations is exceeded, we go
       back to **Step 2**.

**Output of $\mathcal A$:** The adjusted parameters $w\in\mathbb R^d$ and
$b\in\mathbb R$ and therefore a "hopefully" better hypothesis
\begin{align}
  h_{(w,b)} \in\mathcal H.
\end{align}

The most interesting part is of course the following:

**Update Rule:** Compute the difference
\begin{align}
  \Delta^{(i)} &= y^{(i)} - y \\
  &= y^{(i)} - h_{(w,b)}(x^{(i)})
\end{align}
and adjust the parameters $w,b$ as follows
\begin{align}
  w & \leftarrowtail w^\text{new} := w + \eta \Delta^{(i)} x^{(i)}\\
  b & \leftarrowtail b^\text{new} := b + \eta \Delta^{(i)}\\
\end{align}
where $\eta\in\mathbb R^+ \backslash \{0\}$ is a fixed, so-called *learning rate*.

Why does this update rule have a good chance to "improve" the parameter choices
for $w,b$ each time a misclassification is detected?

In view of a misclassification, two cases may occur:

* $\Delta^{(i)}=+2$: This means that the actual label is given by $y^{(i)}=+1$
    and the prediction was $h_{(w,b)}(x^{(i)})=-1$, and hence,
    \begin{align}
      w\cdot x^{(i)}+b < 0.
    \end{align}
    After carrying out the update rule, the updated normal vector 
    \begin{align}
      w^\text{new} &= w + \eta \Delta^{(i)} x^{(i)}\\
      &= w + 2\eta x^{(i)}
    \end{align}
    and likewise the displacement parameter
    \begin{align}
      b^\text{new} 
      &= b + \eta\Delta^{(i)} \\
      &= b + 2\eta
    \end{align}
    imply
    \begin{align}
      & \left(w^\text{new}\cdot x^{(i)}+b^\text{new}\right)
      -\left(w\cdot x^{(i)}+b\right)\\
      =&
      \underbrace{\eta}_{>0}
      \,
      \underbrace{\Delta^{(i)}}_{>0}
      \,
      \underbrace{(x^{(i)})^2}_{\geq 0}
      +
      \underbrace{\eta}_{>0}
      \,
      \underbrace{\Delta^{(i)}}_{>0}\\
      >&  0
    \end{align}
    so that $h_{w^\text{new},b^\text{new}}(x^{(i)}) = \text{sign}\left(w^\text{new}\cdot x^{(i)}+b^\text{new}\right)$ has a better chance to
    evaluate to $+1$ instead of $h_{w,b}(x^{(i)})=\text{sign}\left(w\cdot x^{(i)}+b\right)=-1$.
* $\Delta^{(i)}=-2$: 
    👀 This case is analogous, try on your own.

👀
: How will $\eta$ affect the learning?

In summary, we have set up an algorithm and gave a heuristic argument why,
after sufficiently many iterations, it might provide a good choice for a
hypothesis $h$ to predict the class labels $y=h(x)\in\mathcal Y$ for any new
measurements $x\in \mathcal X$. In other words, a first model of supervised
machine learning.

Our next steps are:

* A Python implementation;
* An analysis of its prediction performances on the Iris data set;
* A proof of convergence of the iteration of the Perceptron algorithm in finite
    many steps should the training data be linearly separable.
    ➢ Next session!


### A more romantic introduction

The more romantic picture behind the Perceptron model is to consider it as a
mathematical caricature of a neuron cell:

![A caricature of a neuron ([Source](https://en.wikipedia.org/wiki/Neuron)).](02/neuron.png)

* The feature vector $\mathbb R^d$ provides the input stimulus at the
    dendrites;
* The coefficients $w_j$ in the weight vector $w\in\mathbb R^d$ model the
    sensitivity of the dendrites;
* The displacement term $b$ models an inherent bias on the neuron;
* The *activation function* $\text{sign}(\cdot)$ maps the *activation*
    \begin{align}
      z(x)=w\cdot x+b
    \end{align}
    of the axon to a "clean" signal
    \begin{align}
      y=\text{sign}(z(x))=h(x) \in \{-1,+1\}.
    \end{align}

It goes without saying that this is a very simple model of a neuron,
nevertheless as we will see, with some minor modifications (leading to the
Adaline neuron), a very versatile and effective building block of artificial neural
networks employed even in modern deep learning techniques. One often calls the
Perceptron a "first generation" and the Adaline a "second generation"
artificial neuron.  Despite their effectiveness, they do not allow to model
some known phenomena of biological neurons. One obvious limitation of the
model is that information can only be encoded by "amplitude modulation" in the
activation function.  Biological neurons, however, seem to encode information
also in the "frequency modulation" of their firing rates. The latter may have
physiological explanations but even without them one can imagine a frequency
modulation may be more robust in noisy environments. 

Be that as it may, by now there is a "third generation", the so-called
[*spiking neurons*](https://en.wikipedia.org/wiki/Spiking_neural_network) that
provide the first more sophisticated mathematical models to model properties
such as amplitudes, frequencies, delays, thresholds in the activation
functions.  Unfortunately, yet no general and efficient method of training is
known and the computational efforts is much more costly. However, that is
likely to change as many efforts are currently invested. For example, [Intel's Loihi
chip](https://www.intel.com/content/www/us/en/research/neuromorphic-computing.html),
features a programmable hardware implementation network of 130.000 such spiking
neurons.
