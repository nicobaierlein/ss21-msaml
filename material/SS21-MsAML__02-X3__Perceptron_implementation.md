# Exercise 02-3: Perceptron implementation [[PDF](SS21-MsAML__02-X3_Perceptron_implementation.pdf)]

Implement the Perceptron algorithm laid out in the [lecture
notes](SS21-MsAML__02-1__Linear_classification.pdf)
on page 9-14 and inspect its performance on the Iris data set based on the
Python tools developed in [Exercise
02-2](SS21-MsAML__02-X2_Preparation_of_Python_tools_for_linear_classification.pdf),
in which you adjusted the weight $w$ and the bias $b$ manually. Let now the
computer do the work of finding a good choice of these parameters. If you
have shuffled the data beforehand, you do not need to randomly choose the data
points in the inner loop. Why is shuffling sensible at all?  Observe how the
learning rate $\eta$ influences the learning performance.


--- 

