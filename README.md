# Table of contents

[[_TOC_]]


# Mathematic(al Statistic)s and Applications of Machine Learning

* Lecture of summer semester 2021
* [Department of Mathematics](https://www.math.lmu.de) at the 
  [LMU Munich](https://www.en.uni-muenchen.de)
* Lecturer [D.-A. Deckert](http://www.mathematik.uni-muenchen.de/~deckert)
* Assistant [M. Seleznova](https://www.ai.math.uni-muenchen.de/members/phd_students/seleznova/index.html)
* [UNI2WORK Course Site](https://uni2work.ifi.lmu.de/course/S21/MI/M%28S%29AML)


## Announcements

* Organization announcements will always be published on the [UNI2WORK homepage of this course](https://uni2work.ifi.lmu.de/course/S21/MI/M%28S%29AML).


## Lectures, exercises, solutions

* Each week the course material including lectures/exercises/solutions for both
* versions will be gathered and linked here: 

  **[>>> Course Material on GitLab <<<](./material/index.md)**

  **[>>> Course Material on Webpage <<<](https://dirk-deckert-lmu.gitlab.io/ss21-msaml/)**

As the course comes in two versions, see explanation in
[UNI2WORK](https://uni2work.ifi.lmu.de/course/S21/MI/M%28S%29AML), some
resources may only be relevant for one of these versions. In this case, the
relevance for the particular version is specified as follows:

* *M* for the *"Mathematics and Applications of Machine Learning"* version
* *S* for the *"Mathematical Statistics and Applications of Machine
Learning"* version

No specification means that the material is relevant for both versions.


## Group discussions

* GitLab offers a light-weight platform that we can exploit to discuss jointly
  the lecture/exercise/solution material by means of so-called “issues”. 
* To access the issue-tracking platform you need to request access to the
  repository logged in with your (free and to be created) GitLab account 
  using your first and last name and email address as specified in your
  UNI2WORK enrollment: see our 
  [get-to-know session](material/SS21-MsAML_01-1_Get-to-know_session.pdf).
* A discussion on a certain topic can be initiated by means of creating an
  issue [here](https://gitlab.com/dirk-deckert-lmu/ss21-msaml/-/issues): 
  * Describe your topic/question/problem/etc. to be discussed in a clear manner
    indicating what your expectation of the outcome of the discussion is.
  * Each such issue can be written in 
    [GitLab Markdow syntax](https://docs.gitlab.com/ee/user/markdown.html) 
    that even includes basic 
    [LaTeX support](https://docs.gitlab.com/ee/user/markdown.html#math) 
    for typing mathematical expressions.
  * Each issue can be voted up or down in priority by everyone in the class. We
    will interpret the number of *votes as a measure of urgency of the issue
    for our group* instead of a measure of quality and content. 
  * Each week I will try to respond to as many of the issues as my time
    constraints permit, either in the thread directly or by leaving a link to
    additional material.
  * I invite everyone to contribute to the discussions, especially for issues
    that I cannot answer in a timely manner. However, only on condition of one
    ground rule: 
    > *Be polite, supportive, and respectful towards each other at all times.*
    >
    Course members not adhering to this ground rule may be banned without
    warning.
  * The goal of the discussion is to improve as a group and if we exploit it
    well, we will spend less time on monologues in lectures and more time on
    group discussions solving exercises and implementing Python examples. So
    before submitting an issue or answering to one, a good etiquette is to ask
    yourself whether your presentation is both:
    * *comprehensive:* 
      * Has the reader of the issue you are reporting a good chance to quickly
        understand the topic and your expectation without additional
        interpretation or detective work? 
      * Is your answer comprehensive and does it meet the expectation of the
        reporter of the issue? 
      * Did you give all necessary references, e.g., to the course material?
        E.g., *"[...] as defined/proved in ??? of lecture ???"*. For this, it
        is convenient to use the 
        [markdown links](https://docs.gitlab.com/ee/user/markdown.html#links) 
        to include hyperlinks.
      * If for some reason you cannot comprehensively meet the expectation, 
        make this explicit in your answer. A partly answer can already be
        helpful if presented as such, e.g., also for the reporter to make the
        expectation more precise.
    * *concise:* 
      * Is the question or answer to the point or can it be shortened?
      * Can you make use of 
      * [LaTeX support](https://docs.gitlab.com/ee/user/markdown.html#math) 
        to make the presentation more precise and less lengthy?
      * Are the references to the point or might they sidetrack the reader?
      * Are the employed adjective, adverbs, or descriptive phrase
        necessary or do they just dilute the precision of your presentation? 
        Many famous phrases, such as *"it can be seen easily that [...]"* and
        the like, may not not contain relevant information for the reporter of
        the issue, and, as in the majority of published papers, rather signal a
        potential gap in the proof or argument.
    * Specify a topic and link to the course material if applicable.
    * And finally, define acceptance criteria for your issue to ensure that the
      answers are to the point.
    * All these efforts will help your issue to receive more up-votes and, in
      turn, also be answered more quickly.
    * As an initial help, you are encouraged to use the provided issue template
      and fill out the placeholders *"[...]"*.
    * This being said, take it as a guide but do not hesitate to report issues
      in any form.


## Course description

This course will provide an introduction into selected topics on machine
learning. We will start from the basics, e.g., perceptrons, adalines, support
vector machines, and proceed to multi-layered neural networks and deep
learning. The minimum goal is to arrive at an understanding of both the
mathematics and the implementation of the now standard "handwritten numbers
recognition" problem by means of neural networks. The mathematical discussion
will focus on machine learning as an statistical optimization and approximation
problem. As regards applications, it is the objective of the tutorials to
implement several applications of the discussed algorithms in Python. Depending
on interest and time, we may select from more advanced topics such as vector
embeddings, convolution and recurrent networks, decision trees, reinforcement
learning, ensemble methods, and boosting.


### Target group and prior knowledge

This course is offered having mainly master students in the programmes
Mathematics, Financial Mathematics, and Theoretical and Mathematical Physics at
LMU Munich with corresponding prior knowledge, in particular, also in
Probability Theory in mind.

A basic knowledge in Python or a similar programming language and access to a
computer with a Python development environment is required in order to complete
the tutorials.


## Course style

My area of research is mathematical physics. So why this lecture?  My intention
is this: Contrary to computer science, mathematics had a later start in the
field of machine learning. This is reflected in the disproportionally large
number of books on applications of machine learning compared to the one on
its mathematical foundation. Naturally, the mathematics textbooks focus on
theory only. We want to contribute with something in between mathematics and
applications. Therefore, we have tried to select course material to bridge
mathematics, statistics, and applications, while advertising the field in our
more "mathematical jargon". 


### Course material and standard references

This lecture will not follow one particular text-book. Rather, we will pick out
topics from various sources and I will try to cite all relevant sources to the
best of my knowledge while keeping the course material for our purposes
self-comprehensively. Please let me know if appropriate citations of books,
papers, source code, media files, etc., are missing, in which case I will
gladly add them. 

For starters, here are some standard references for your literature review from
which I will select most of the material:

**Algorithms:**

* [Artificial Intelligence: A Modern Approach. Stuart J. Russell, Peter Norvig, Prentice Hall, 2010.](https://www.pearson.com/store/p/artificial-intelligence-a-modern-approach/P100000667303/9780136042594)

* [Pattern Recognition and Machine Learning. Christopher Bishop, Springer, 2006.](https://www.springer.com/de/book/9780387310732)

**Mathematics:**

* [Statistical Learning Theory. Vladimir N. Vapnik, Wiley, 1998.](https://www.wiley.com/en-us/Statistical+Learning+Theory-p-9780471030034)

* [Foundations of Machine Learning. Mehryar Mohri, Afshin Rostamizadeh, Ameet Talwalkar, MIT Press, 2018.](https://mitpress.mit.edu/books/foundations-machine-learning-second-edition)

* [Understanding Machine Learning: From Theory to Algorithms. Shai Shalev-Shwartz Shai Ben-David, Cambridge, 2014.](https://www.cambridge.org/core/books/understanding-machine-learning/3059695661405D25673058E43C8BE2A6)

* [Statistisches und maschinelles Lernen: Gängige Verfahren im Überblick. Stefan Richter, Springer, 2019.](https://www.springer.com/de/book/9783662593530)

* [Optimierung. Florian Jarre, Josef Stoer, Springer, 2019.](https://www.springer.com/de/book/9783662588543)

**Python Implementations:**

* [Python Machine Learning. Sebastian Raschka, Vahid Mirjalili, Packt, 2019.](https://www.packtpub.com/data/python-machine-learning-third-edition)

* [Neural networks and Deep Learning, Michael Nielson, 2019.](http://neuralnetworksanddeeplearning.com/)

* [Hands-On Machine Learning with Scikit-Learn, Keras, and TensorFlow, Aurélien Géron, O'Reilly, 2019.](https://www.oreilly.com/library/view/hands-on-machine-learning/9781492032632/)

**Python Language:**

* Probably the most well-written and efficient tour through the Python language: 

  [Python Tutorial](https://docs.python.org/3/tutorial/index.html)
  
  Note that many of the chapters can be read independently, which also makes it a great
  resource to quickly look-up and recall some functionality.


# License

* [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)
* [LICENSE file](https://gitlab.com/dirk-deckert-lmu/maml-ss20/-/blob/master/LICENSE)
